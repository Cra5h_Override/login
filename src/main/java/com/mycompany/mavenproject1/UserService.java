/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserService {

    public static ArrayList<User> getList() {
        return list;
    }
    
    private static ArrayList<User> list = new ArrayList<User>();
    public static void load(){
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("user.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            list = (ArrayList<User>)ois.readObject();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(testReadFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(testReadFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(testReadFile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void save(ArrayList<User> list){
        UserService.list=list;
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;;
        try {
            file = new File("user.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(list);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(testWriteFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(testWriteFile.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(testWriteFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    public static boolean auth(String loginName,String password){
        for(User user:list){
            if (user.getLoginName().equals(loginName)&&user.getPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }
}
